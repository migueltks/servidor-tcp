package com.ks.tools.servidor;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.Servidor;
import com.ks.lib.tcp.Tcp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class App
{
    private static final Logger VMobjLog = LogManager.getLogger(App.class);

    public static void main(String[] args)
    {
        System.out.println("Iniciando la aplicacion\n");

        try
        {
            int port = Integer.parseInt(System.getProperty("Port"));

            if (port > 0)
            {
                String file = System.getProperty("file");
                String type = System.getProperty("ConnectionType");
                Tcp connection;
                Comunications data = new Comunications();
                if (type.equals("client"))
                {
                    String ip = System.getProperty("IPClient");
                    System.out.println("Tipo de conexion: Cliente");
                    connection = new Cliente();
                    connection.setIP(ip);
                    System.out.println("IP: " + ip);
                }
                else
                {
                    System.out.println("Tipo de conexion: Servidor");
                    connection = new Servidor();
                }
                System.out.println("Puerto: " + port);
                connection.setPuerto(port);
                connection.setEventos(data);
                data.setConexion(connection);
                if (file != null)
                {
                    if (file.length() > 0)
                    {
                        File archivo = new File(file);
                        if (archivo.exists())
                        {
                            System.out.println("Leer el archivo: " + file);
                            data.setFile(file);
                        }
                        else
                        {
                            System.out.println("WARN :\tNo se encontro el archivo, la aplicacion se va a levantar sin el envio de información.");
                        }
                    }
                }
                try
                {
                    connection.conectar();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            Thread.currentThread().sleep(1000);
        }
        catch (Exception ex)
        {
            System.out.printf("Problema al iniciar la aplicacion", ex);
        }
    }
}
