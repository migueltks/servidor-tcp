package com.ks.tools.servidor;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;
import com.ks.lib.tcp.Tcp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by migue on 16/10/2015.
 */
public class Comunications implements EventosTCP
{
    private static final Logger VMobjLog = LogManager.getLogger(Comunications.class);

    private String VMstrFile;
    private BufferedReader VMioFile;
    private int VMintContador;
    private int VMintContEnvio;
    private Tcp conexion;
    private Thread VMthdProcesar;

    public Comunications()
    {
        VMstrFile = "";
        VMintContador = 0;
        VMthdProcesar = new Thread();
    }

    public void setFile(String file)
    {
        VMstrFile = file;
    }

    public void openFile()
    {
        try
        {
            System.out.println("Iniciando el envio de información del archivo " + VMstrFile);
            VMioFile = new BufferedReader(new FileReader(VMstrFile));
            if (VMioFile != null)
            {
                String line;
                VMintContEnvio = 0;
                while ((line = VMioFile.readLine()) != null)
                {
                    if (conexion != null)
                    {
                        conexion.enviar(line);
                        VMintContEnvio++;
                        System.out.println("Linea enviada " + VMintContEnvio);
                        System.out.println(line);
                    }
                }
            }
        }
        catch (Exception e)
        {
            System.out.printf("Problema al enviar la información del archivo", e);
        }
    }

    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println("Se realizo una conexion - " + cliente.toString());
        if (VMstrFile.length() > 0)
        {
            if (!VMthdProcesar.isAlive())
            {
                VMthdProcesar = new Thread()
                {
                    @Override
                    public void run()
                    {
                        openFile();
                    }
                };
                VMthdProcesar.start();
            }
        }
    }

    public void errorConexion(String s)
    {
        System.out.println("Problema con la conexion: " + s + " se reintentara en 1 minuto");
    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println("Mensaje recibido: " + VMintContador);
        System.out.println(s);
    }

    public void cerrarConexion(Cliente cliente)
    {
        System.out.printf("Se cerro la conexion: " + cliente.toString());
    }

    public void setConexion(Tcp conexion)
    {
        this.conexion = conexion;
    }
}
